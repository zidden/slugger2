<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
</head>
<body>
    <nav>
        <div class="container row">
            @if (Route::has('login'))
            
                @auth
                <div class="col col1">
                    <a href="{{ url('/') }}"><img src="{{asset('svg/home-solid.svg')}}" class="ico" style="width:10px;"> Home</a>
                </div>
                
                @else
                <div class="col col1">    
                    <a href="{{ route('login') }}"><img src="{{asset('svg/sign-in-alt-solid.svg')}}" class="ico" style="width:10px;"> Login</a>
                </div>
                <div class="col col1">
                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                </div>
                @endauth
                
            
            @endif
            <div class="col col1">
            <a href="{{route('carti')}}"><img src="{{asset('svg/shopping-cart-solid.svg')}}" class="ico" style="width:10px;"> Cart</a>
            </div>
            @auth
                <div class="col col1">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
    
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                    
                </div>
                @endauth
        </div>
    </nav>
    <main class="py-4">
        @yield('content')
    </main>
</body>
</html>
