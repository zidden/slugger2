<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
    protected $table = 'cart';
    protected $foreignKey = 'Kode';
    public $incrementing = false;
    public $timestamps = false;

    public static function getTotalHarga(){
        return Cart::sum('Jumlah_Harga');
    }
}
